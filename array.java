package test_2608;

import java.util.Scanner;
import java.util.*;

public class array {
    public static void main(String[] args) {



        Scanner sc =new Scanner(System.in);
        System.out.println("Enter the number of elements");
        int n=sc.nextInt();
        int arr[]=new int[n];

        System.out.println("Enter the elements");
        for(int i=0;i<n;i++)
        {
            arr[i]=sc.nextInt();
        }

        System.out.println("The array elements are");
        for( int i=0;i<n;i++)
        {
            System.out.println(arr[i]);
        }

        Arrays.sort(arr);
        System.out.println("The sorted array elements are");
        for( int i=0;i<n;i++)
        {
            System.out.println(arr[i]);
        }
        System.out.println("The third largest number is "+arr[(arr.length)-3]);

        System.out.println("The elements on odd index are");
        for(int i=0;i<n;i++)
        {
            if(i%2!=0)
            {
                System.out.println(arr[i]);
            }
        }
    }
}
